# README #

This is a simple application I built to help track how I spend my time.  The aim is simplicity and usability over a perfect app.  That said, improvements are always welcome.

### How to use the application: ###

* Check or uncheck rows to start the timer (only one timer running at a time for now).
* Add descriptions to track specific tasks.
* To switch tasks, check the row of the task you're switching to.

### How do I get set up? ###

* Build the code.
* Add clock_timer.ico to the location where you have the binaries.
* Run the executable

### Contribution guidelines ###

* Fork and pull request.

### Questions? ###

* Email my account name at Google's email site.