﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TaskTimer
{
    public class StopwatchTimer : Timer
    {
        private StopwatchWithOffset stopwatch;

        public StopwatchTimer()
        {
            this.stopwatch = new StopwatchWithOffset();
        }

        public StopwatchTimer(TimeSpan offset)
        {
            this.stopwatch = new StopwatchWithOffset(offset);
        }

        public TimeSpan Elapsed
        {
            get { return this.stopwatch.Elapsed; }
            set { this.stopwatch.Elapsed = value; }
        }

        public long ElapsedTicks
        {
            get { return this.stopwatch.ElapsedTicks; }
            set { this.stopwatch.ElapsedTicks = value; }
        }

        public bool IsRunning
        {
            get { return this.stopwatch.IsRunning; }
        }

        public new void Start()
        {
            base.Start();
            this.stopwatch.Start();
        }

        public static StopwatchTimer StartNew(TimeSpan offset)
        {
            var stopWatchTimer = new StopwatchTimer(offset);
            stopWatchTimer.Start();
            return stopWatchTimer;
        }

        public new void Stop()
        {
            this.stopwatch.Stop();
            base.Stop();
        }
    }
}
