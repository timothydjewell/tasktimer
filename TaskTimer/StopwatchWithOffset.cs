﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TaskTimer
{
    public class StopwatchWithOffset
    {
        TimeSpan offset;
        Stopwatch stopwatch;

        public StopwatchWithOffset()
        {
            this.stopwatch = new Stopwatch();
        }

        public StopwatchWithOffset(TimeSpan offset)
            : this()
        {
            this.offset = offset;
        }

        public void Start()
        {
            this.stopwatch.Start();
        }

        public void Stop()
        {
            this.stopwatch.Stop();
        }

        public TimeSpan Elapsed
        {
            get
            {
                return this.stopwatch.Elapsed + this.offset;
            }

            set
            {
                this.offset = value;
                this.ResetOrRestart();
            }
        }

        public long ElapsedTicks
        {
            get
            {
                return this.stopwatch.ElapsedTicks + this.offset.Ticks;
            }

            set
            {
                this.offset = new TimeSpan(value);
                this.ResetOrRestart();
            }
        }

        public bool IsRunning
        {
            get { return this.stopwatch.IsRunning; }
        }

        private void ResetOrRestart()
        {
            if (this.stopwatch.IsRunning)
            {
                this.stopwatch.Restart();
            }
            else
            {
                this.stopwatch.Reset();
            }
        }
    }
}
