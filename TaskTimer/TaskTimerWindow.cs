﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace TaskTimer
{
    public partial class TaskTimerWindow : Form
    {
        private const string TimeSpanFormat = @"h\:mm\:ss";
        private Label TotalTimeLabel;
        private StopwatchTimer runningTimer;
        private DataGridViewTextBoxCell runningTimerCell;
        private Button clearTimingsBtn;
        private Button clearTimersBtn;
        private DataGridView TimingTableGrid;
        private DataGridViewCheckBoxColumn ActiveColumn;
        private DataGridViewTextBoxColumn TimeSpanColumn;
        private DataGridViewTextBoxColumn DescriptionColumn;

        public TaskTimerWindow()
        {
            this.TotalTimeLabel = new Label();
            this.TotalTimeLabel.Location = new System.Drawing.Point(560, 10);
            this.TotalTimeLabel.Size = new System.Drawing.Size(50, 20);
            this.TotalTimeLabel.TextAlign = ContentAlignment.MiddleRight;
            this.TotalTimeLabel.Text = TimeSpan.Zero.ToString(TimeSpanFormat);
            this.Controls.Add(this.TotalTimeLabel);

            InitializeTable();

            this.clearTimingsBtn = new Button();
            this.clearTimingsBtn.Location = new System.Drawing.Point(10, 10);
            this.clearTimingsBtn.Size = new System.Drawing.Size(80, 25);
            this.clearTimingsBtn.Text = "Clear Timings";
            this.clearTimingsBtn.UseVisualStyleBackColor = true;
            this.Controls.Add(this.clearTimingsBtn);
            this.clearTimingsBtn.Click += (o1, e1) =>
                {
                    ClearRunningTimer();
                    foreach (DataGridViewRow row in this.TimingTableGrid.Rows)
                    {
                        row.Cells[this.TimeSpanColumn.Index].Value = TimeSpan.Zero;
                    }
                };

            this.clearTimersBtn = new Button();
            this.clearTimersBtn.Location = new System.Drawing.Point(100, 10);
            this.clearTimersBtn.Size = new System.Drawing.Size(80, 25);
            this.clearTimersBtn.Text = "Clear Timers";
            this.clearTimersBtn.UseVisualStyleBackColor = true;
            this.Controls.Add(this.clearTimersBtn);
            this.clearTimersBtn.Click += (o2, e2) =>
                {
                    this.ClearRunningTimer();
                    this.TimingTableGrid.Rows.Clear();
                };

            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 320);
            this.clearTimersBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            this.clearTimingsBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left;
            this.TotalTimeLabel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            this.TimingTableGrid.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.Name = "TaskTimerWindow";
            this.Text = "TaskTimer";
            string iconFilePath = "timer_clock.ico";
            if (System.IO.File.Exists(iconFilePath))
            {
                this.Icon = new Icon(iconFilePath);
            }
        }

        private void InitializeTable()
        {
            this.TimingTableGrid = new DataGridView();
            this.ActiveColumn = new DataGridViewCheckBoxColumn();
            this.TimeSpanColumn = new DataGridViewTextBoxColumn();
            this.DescriptionColumn = new DataGridViewTextBoxColumn();
            ((ISupportInitialize)(this.TimingTableGrid)).BeginInit();
            this.SuspendLayout();
            this.TimingTableGrid.AllowUserToOrderColumns = true;
            this.TimingTableGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TimingTableGrid.Columns.AddRange(new DataGridViewColumn[]
            {
                this.ActiveColumn,
                this.TimeSpanColumn,
                this.DescriptionColumn
            });
            this.TimingTableGrid.Location = new Point(10, 45);
            this.TimingTableGrid.Name = "TimingTableGrid";
            this.TimingTableGrid.Size = new Size(600, 265);
            this.TimingTableGrid.TabIndex = 3;

            this.ActiveColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;

            this.TimeSpanColumn.HeaderText = "Time";
            this.TimeSpanColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            //this.Time.ReadOnly = true;
            this.TimeSpanColumn.ValueType = typeof(TimeSpan?);
            this.TimeSpanColumn.Width = 50;

            this.DescriptionColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            this.DescriptionColumn.HeaderText = "Description";
            this.DescriptionColumn.Name = "DescriptionColumn";
            this.DescriptionColumn.DefaultCellStyle.WrapMode = DataGridViewTriState.True;

            this.Controls.Add(this.TimingTableGrid);
            ((ISupportInitialize)(this.TimingTableGrid)).EndInit();
            this.ResumeLayout(false);
            this.TimingTableGrid.CellContentClick += TimingTableGridCellContentClick;
            this.TimingTableGrid.CellFormatting += (o, e) =>
                {
                    if (e.ColumnIndex == this.TimeSpanColumn.Index)
                    {
                        if (e.Value == null)
                        {
                            e.Value = TimeSpan.Zero;
                        }

                        e.Value = ((TimeSpan)e.Value).ToString(TimeSpanFormat);
                        e.FormattingApplied = true;
                    }
                };
        }

        private void TimingTableGridCellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == this.ActiveColumn.Index)
            {
                var isActiveCell = (DataGridViewCheckBoxCell)this.TimingTableGrid[this.ActiveColumn.Index, e.RowIndex];
                if (this.runningTimerCell != null)
                {
                    if (this.runningTimerCell.RowIndex == e.RowIndex)
                    {
                        if (IsChecked(isActiveCell))
                        {
                            this.runningTimer.Start();
                        }
                        else
                        {
                            this.runningTimer.Stop();
                        }

                        return;
                    }

                    this.runningTimer.Stop();
                    this.TimingTableGrid[this.ActiveColumn.Index, this.runningTimerCell.RowIndex].Value = false;
                }

                this.runningTimerCell = (DataGridViewTextBoxCell)this.TimingTableGrid[this.TimeSpanColumn.Index, e.RowIndex];
                TimeSpan existingTimeSpan = (TimeSpan?)this.runningTimerCell.Value ?? TimeSpan.Zero;

                this.runningTimer = StopwatchTimer.StartNew(existingTimeSpan);
                TimeSpan otherCellsSum = new TimeSpan(
                    this.TimingTableGrid.Rows.OfType<DataGridViewRow>()
                    .Where(row => row.Index != e.RowIndex)
                    .Select(row => (TimeSpan?)row.Cells[this.TimeSpanColumn.Index].Value ?? TimeSpan.Zero)
                    .Sum(t => t.Ticks));

                this.runningTimer.Tick += (obj, ev) =>
                    {
                        this.runningTimerCell.Value = this.runningTimer.Elapsed;
                        this.TotalTimeLabel.Text = (otherCellsSum + this.runningTimer.Elapsed).ToString(TimeSpanFormat);
                    };
            }
        }

        private void ClearRunningTimer()
        {
            if (this.runningTimerCell != null)
            {
                this.TimingTableGrid.Rows[this.runningTimerCell.RowIndex].Cells[this.ActiveColumn.Index].Value = false;
                this.runningTimer.Stop();
                this.runningTimer = null;
                this.runningTimerCell = null;
            }
        }

        private static bool IsChecked(DataGridViewCell c)
        {
            return c.Value != null && (bool)c.Value;
        }
    }
}
